package hu.pont.mompark.ui.fragment


import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import hu.pont.mompark.R
import yalantis.com.sidemenu.interfaces.ScreenShotable
import android.graphics.Bitmap
import android.support.v4.app.Fragment
import android.graphics.Canvas
import com.vlad.android.kotlin.async
import android.widget.Button
import butterknife.bindView
import com.dd.CircularProgressButton
import android.animation.ValueAnimator
import android.view.animation.AccelerateDecelerateInterpolator
import hu.pont.mompark.widget.ExpandableHeightGridView
import android.widget.BaseAdapter
import android.widget.ListAdapter
import hu.pont.mompark.adapter.KuponChooseAdapter
import hu.pont.mompark.model.Uzlet
import android.widget.TextView
import android.widget.LinearLayout
import android.view.animation.RotateAnimation
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.widget.ImageView

public class ProfilFragment : Fragment(), ScreenShotable, View.OnClickListener {

    private var containerView: View? = null

    private var bitmap: Bitmap? = null

    val kuponYesBtn: Button by bindView(R.id.kuponYesBtn)
    val kuponNoBtn: Button by bindView(R.id.kuponNoBtn)
    val kuponNotiYesBtn: Button by bindView(R.id.kuponNotiYes)
    val kuponNotiNoBtn: Button by bindView(R.id.kuponNotiNemBtn)
    val saveButton: CircularProgressButton by bindView(R.id.circularButton1)
    val kuponokGridView: ExpandableHeightGridView by bindView(R.id.osszetevokGridView)
    val kuponHeaderTextView: TextView by bindView(R.id.textView5)
    val kuponPickerLayout: LinearLayout by bindView(R.id.osszetevoLayout)
    val arrowImageview: ImageView by bindView(R.id.arrowImageView)

    val notiArrowImageView: ImageView by bindView(R.id.notiImageView)
    val threeDayNotiButton: Button by bindView(R.id.threenapontaButton)
    val hetenteButton: Button by bindView(R.id.hetenteButton)
    val havontaButton: Button by bindView(R.id.havontaButton)
    val notiLinearLayout: LinearLayout by bindView(R.id.notifierLinearLayout)
    val notiHeaderTextView: TextView by bindView(R.id.notiHeaderTextView)

    var isKuponPickerExpanded: Boolean = false
    var isNotifierLayoutExpanded: Boolean = false

    override fun takeScreenShot() {
        async {
            val bitmap = Bitmap.createBitmap(containerView!!.getWidth(), containerView!!.getHeight(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            containerView!!.draw(canvas)
            this@ProfilFragment.bitmap = bitmap
        }
    }

    override fun getBitmap(): Bitmap? {
        return bitmap
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super<Fragment>.onViewCreated(view, savedInstanceState)
        this.containerView = view.findViewById(R.id.container)

        kuponYesBtn!!.setOnClickListener(this);
        kuponNoBtn!!.setOnClickListener(this);
        kuponNotiNoBtn!!.setOnClickListener(this);
        kuponNotiYesBtn!!.setOnClickListener(this);
        havontaButton!!.setOnClickListener(this);
        threeDayNotiButton!!.setOnClickListener(this);
        hetenteButton!!.setOnClickListener(this);

        closeKuponLayout()
        closeNotifierLayout()

        notiHeaderTextView!!.setOnClickListener { view ->
            if(!isNotifierLayoutExpanded){
                expandNotifierLayout()
            }else{
                closeNotifierLayout()
            }
        }

        kuponHeaderTextView!!.setOnClickListener { view ->
            if(!isKuponPickerExpanded){
                expandKuponLayout()
            }else{
                closeKuponLayout()
            }
        }

        saveButton!!.setOnClickListener { view ->
            if(saveButton.getProgress() == 0){
                simulateSuccesProgress(saveButton)
            }else{
                saveButton.setProgress(0)
            }
        }

        kuponokGridView.setAdapter(KuponChooseAdapter(Uzlet.boltTipusLista, getActivity()))
        kuponokGridView.setExpanded(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profil, container, false)
    }

    override fun onClick(v: View) {
        when(v.getId()){
            R.id.kuponYesBtn -> toogleKuponButtonYes()
            R.id.kuponNoBtn -> toogleKuponButtonNo()
            R.id.kuponNotiYes ->toogleKuponNotiYes()
            R.id.kuponNotiNemBtn -> toogleKuponNotiNo()
            R.id.hetenteButton -> toogleHetente()
            R.id.havontaButton -> toogleHavonta()
            R.id.threenapontaButton -> toogleThreeNapontaNoti()
        }
    }

    fun expandKuponLayout(){
        kuponPickerLayout.setVisibility(View.VISIBLE)
        isKuponPickerExpanded = true

        var rotateAnimation: RotateAnimation = RotateAnimation(0F, -90F, Animation.RELATIVE_TO_SELF, 0.5F, Animation.RELATIVE_TO_SELF, 0.5F);
        rotateAnimation!!.setInterpolator(LinearInterpolator())
        rotateAnimation!!.setDuration(250L)
        rotateAnimation!!.setFillAfter(true)
        arrowImageview.startAnimation(rotateAnimation)
    }

    fun closeKuponLayout(){
        kuponPickerLayout.setVisibility(View.GONE)
        isKuponPickerExpanded = false

        var rotateAnimation: RotateAnimation = RotateAnimation(-90F, 0F, Animation.RELATIVE_TO_SELF, 0.5F, Animation.RELATIVE_TO_SELF, 0.5F);
        rotateAnimation!!.setInterpolator(LinearInterpolator())
        rotateAnimation!!.setDuration(250L)
        rotateAnimation!!.setFillAfter(true)
        arrowImageview.startAnimation(rotateAnimation)
    }

    fun expandNotifierLayout(){
        notiLinearLayout.setVisibility(View.VISIBLE)
        isNotifierLayoutExpanded = true

        var rotateAnimation: RotateAnimation = RotateAnimation(0F, -90F, Animation.RELATIVE_TO_SELF, 0.5F, Animation.RELATIVE_TO_SELF, 0.5F);
        rotateAnimation!!.setInterpolator(LinearInterpolator())
        rotateAnimation!!.setDuration(250L)
        rotateAnimation!!.setFillAfter(true)
        notiArrowImageView.startAnimation(rotateAnimation)
    }

    fun closeNotifierLayout(){
        notiLinearLayout.setVisibility(View.GONE)
        isNotifierLayoutExpanded = false

        var rotateAnimation: RotateAnimation = RotateAnimation(-90F, 0F, Animation.RELATIVE_TO_SELF, 0.5F, Animation.RELATIVE_TO_SELF, 0.5F);
        rotateAnimation!!.setInterpolator(LinearInterpolator())
        rotateAnimation!!.setDuration(250L)
        rotateAnimation!!.setFillAfter(true)
        notiArrowImageView.startAnimation(rotateAnimation)
    }

    fun toogleKuponButtonYes(){
        kuponYesBtn.setBackgroundColor(getResources().getColor(R.color.mompark_pink))
        kuponYesBtn.setTextColor(getResources().getColor(android.R.color.white))
        kuponNoBtn.setBackgroundColor(getResources().getColor(android.R.color.white))
        kuponNoBtn.setTextColor(getResources().getColor(R.color.mompark_grey))
    }

    fun toogleKuponButtonNo(){
        kuponNoBtn.setBackgroundColor(getResources().getColor(R.color.mompark_pink))
        kuponNoBtn.setTextColor(getResources().getColor(android.R.color.white))
        kuponYesBtn.setBackgroundColor(getResources().getColor(android.R.color.white))
        kuponYesBtn.setTextColor(getResources().getColor(R.color.mompark_grey))
    }

    fun toogleKuponNotiYes(){
        kuponNotiYesBtn.setBackgroundColor(getResources().getColor(R.color.mompark_pink))
        kuponNotiYesBtn.setTextColor(getResources().getColor(android.R.color.white))
        kuponNotiNoBtn.setBackgroundColor(getResources().getColor(android.R.color.white))
        kuponNotiNoBtn.setTextColor(getResources().getColor(R.color.mompark_grey))
    }

    fun toogleKuponNotiNo(){
        kuponNotiNoBtn!!.setBackgroundColor(getResources().getColor(R.color.mompark_pink))
        kuponNotiNoBtn!!.setTextColor(getResources().getColor(android.R.color.white))
        kuponNotiYesBtn!!.setBackgroundColor(getResources().getColor(android.R.color.white))
        kuponNotiYesBtn!!.setTextColor(getResources().getColor(R.color.mompark_grey))
    }

    fun toogleThreeNapontaNoti(){
        threeDayNotiButton!!.setBackgroundColor(getResources().getColor(R.color.mompark_pink))
        threeDayNotiButton!!.setTextColor(getResources().getColor(android.R.color.white))
        havontaButton!!.setBackgroundColor(getResources().getColor(android.R.color.white))
        havontaButton!!.setTextColor(getResources().getColor(R.color.mompark_grey))
        hetenteButton!!.setBackgroundColor(getResources().getColor(android.R.color.white))
        hetenteButton!!.setTextColor(getResources().getColor(R.color.mompark_grey))
    }

    fun toogleHavonta(){
        havontaButton!!.setBackgroundColor(getResources().getColor(R.color.mompark_pink))
        havontaButton!!.setTextColor(getResources().getColor(android.R.color.white))
        threeDayNotiButton!!.setBackgroundColor(getResources().getColor(android.R.color.white))
        threeDayNotiButton!!.setTextColor(getResources().getColor(R.color.mompark_grey))
        hetenteButton!!.setBackgroundColor(getResources().getColor(android.R.color.white))
        hetenteButton!!.setTextColor(getResources().getColor(R.color.mompark_grey))
    }

    fun toogleHetente(){
        hetenteButton!!.setBackgroundColor(getResources().getColor(R.color.mompark_pink))
        hetenteButton!!.setTextColor(getResources().getColor(android.R.color.white))
        havontaButton!!.setBackgroundColor(getResources().getColor(android.R.color.white))
        havontaButton!!.setTextColor(getResources().getColor(R.color.mompark_grey))
        threeDayNotiButton!!.setBackgroundColor(getResources().getColor(android.R.color.white))
        threeDayNotiButton!!.setTextColor(getResources().getColor(R.color.mompark_grey))
    }

    class object {
        public val PROFIL: String = "Profil"
    }

    fun simulateSuccesProgress(button: CircularProgressButton){
        var widthAnimation: ValueAnimator? = ValueAnimator.ofInt(1, 100);
        widthAnimation!!.setDuration(1300)
        widthAnimation!!.setInterpolator(AccelerateDecelerateInterpolator())
        widthAnimation!!.addUpdateListener(
                ValueAnimator.AnimatorUpdateListener { animation ->
                    var value: Int = (animation.getAnimatedValue() as Int)
                    button.setProgress(value)
                }
        )
        widthAnimation!!.start()
    }
}
