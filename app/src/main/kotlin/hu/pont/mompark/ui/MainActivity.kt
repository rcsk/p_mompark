package hu.pont.mompark.ui

import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarActivity
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.widget.LinearLayout

import java.util.ArrayList

import io.codetail.animation.ViewAnimationUtils
import hu.pont.mompark.ui.fragment.ContentFragment
import yalantis.com.sidemenu.util.ViewAnimator
import yalantis.com.sidemenu.model.SlideMenuItem
import yalantis.com.sidemenu.interfaces.Resourceble
import yalantis.com.sidemenu.interfaces.ScreenShotable
import hu.pont.mompark.R
import hu.pont.mompark.ui.fragment.UzletListFragment
import hu.pont.mompark.ui.fragment.KapcsolatFragment
import hu.pont.mompark.ui.fragment.EsemenyekFragment
import hu.pont.mompark.ui.fragment.ProfilFragment
import hu.pont.mompark.ui.fragment.KuponListFragment
import hu.pont.mompark.ui.fragment.VipFragment
import android.support.v4.app.Fragment


public class MainActivity : ActionBarActivity(), ViewAnimator.ViewAnimatorListener {
    private var drawerLayout: DrawerLayout? = null
    private var drawerToggle: ActionBarDrawerToggle? = null
    private val list = ArrayList<SlideMenuItem>()
    private var contentFragment: UzletListFragment? = null
    private var viewAnimator: ViewAnimator<Resourceble>? = null
    private var res = R.drawable.content_music
    private var linearLayout: LinearLayout? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super<ActionBarActivity>.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            contentFragment = UzletListFragment()
            getSupportFragmentManager().beginTransaction().add(R.id.content_frame, contentFragment).commit()
        }
        drawerLayout = findViewById(R.id.drawer_layout) as DrawerLayout
        drawerLayout!!.setScrimColor(Color.TRANSPARENT)
        linearLayout = findViewById(R.id.left_drawer) as LinearLayout
        linearLayout!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                drawerLayout!!.closeDrawers()
            }
        })


        setActionBar()
        createMenuList()
        viewAnimator = ViewAnimator(this, list, contentFragment, drawerLayout, this)
    }

    private fun createMenuList() {
        val menuItem0 = SlideMenuItem(ContentFragment.CLOSE, R.drawable.cross)
        list.add(menuItem0)
        val menuItem = SlideMenuItem(UzletListFragment.UZLETLIST, R.drawable.uzletek)
        list.add(menuItem)
        val menuItem2 = SlideMenuItem(KapcsolatFragment.KAPCSOLAT, R.drawable.elerhetoseg)
        list.add(menuItem2)
        val menuItem3 = SlideMenuItem(EsemenyekFragment.ESEMENYEK, R.drawable.esemenyek)
        list.add(menuItem3)
        val menuItem4 = SlideMenuItem(ProfilFragment.PROFIL, R.drawable.profilom)
        list.add(menuItem4)
        val menuItem5 = SlideMenuItem(KuponListFragment.KUPONLIST, R.drawable.kupon)
        list.add(menuItem5)
        val menuItem6 = SlideMenuItem(VipFragment.VIPFRAGMENT, R.drawable.vip)
        list.add(menuItem6)
    }


    private fun setActionBar() {
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        getSupportActionBar().setHomeButtonEnabled(true)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true)
        drawerToggle = object : ActionBarDrawerToggle(this, /* host Activity */
                drawerLayout, /* DrawerLayout object */
                toolbar, /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open, /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */) {

            /** Called when a drawer has settled in a completely closed state. */
            override fun onDrawerClosed(view: View) {
                super.onDrawerClosed(view)
                linearLayout!!.removeAllViews()
                linearLayout!!.invalidate()
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)
                if (slideOffset > 0.6 && linearLayout!!.getChildCount() == 0)
                    viewAnimator!!.showMenuContent()
            }

            /** Called when a drawer has settled in a completely open state. */
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
            }
        }
        drawerLayout!!.setDrawerListener(drawerToggle)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super<ActionBarActivity>.onPostCreate(savedInstanceState)
        drawerToggle!!.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super<ActionBarActivity>.onConfigurationChanged(newConfig)
        drawerToggle!!.onConfigurationChanged(newConfig)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        getMenuInflater().inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (drawerToggle!!.onOptionsItemSelected(item)) {
            return true
        }

        return super<ActionBarActivity>.onOptionsItemSelected(item)
    }

    private fun replaceFragment(fragment: Fragment, topPosition: Int): ScreenShotable? {
        val view = findViewById(R.id.content_frame)
        val finalRadius = Math.max(view.getWidth(), view.getHeight())
        val animator = ViewAnimationUtils.createCircularReveal(view, 0, topPosition, 0F, finalRadius.toFloat())
        animator.setInterpolator(AccelerateInterpolator())
        animator.setDuration(ViewAnimator.CIRCULAR_REVEAL_ANIMATION_DURATION)

        findViewById(R.id.content_overlay).setBackgroundDrawable(BitmapDrawable(getResources(), (fragment as ScreenShotable).getBitmap()))
        animator.start()

        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment, null).commit()
        return fragment
    }

    override fun onSwitch(slideMenuItem: Resourceble, screenShotable: ScreenShotable, position: Int): ScreenShotable? {
        when (slideMenuItem.getName()) {
            ContentFragment.CLOSE -> return screenShotable
            UzletListFragment.UZLETLIST -> return replaceFragment(UzletListFragment(), position)
            KapcsolatFragment.KAPCSOLAT -> return replaceFragment(KapcsolatFragment(), position)
            EsemenyekFragment.ESEMENYEK -> return replaceFragment(EsemenyekFragment(), position)
            ProfilFragment.PROFIL -> return replaceFragment(ProfilFragment(), position)
            KuponListFragment.KUPONLIST -> return replaceFragment(KuponListFragment(), position)
            VipFragment.VIPFRAGMENT -> return replaceFragment(VipFragment(), position)
        }
        return screenShotable
    }

    override fun disableHomeButton() {
        getSupportActionBar().setHomeButtonEnabled(false)
    }

    override fun enableHomeButton() {
        getSupportActionBar().setHomeButtonEnabled(true)
        drawerLayout!!.closeDrawers()

    }

    override fun addViewToContainer(view: View) {
        linearLayout!!.addView(view)
    }
}
