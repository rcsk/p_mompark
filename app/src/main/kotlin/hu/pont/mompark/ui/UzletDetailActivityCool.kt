package hu.pont.mompark.ui


import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks
import com.github.ksoichiro.android.observablescrollview.ScrollState
import com.github.ksoichiro.android.observablescrollview.ScrollUtils
import com.nineoldandroids.view.ViewHelper
import com.nineoldandroids.view.ViewPropertyAnimator

import hu.pont.mompark.R
import android.text.Html
import hu.pont.mompark.model.Uzlet
import android.widget.ImageView

/**
 * Created by Richard Radics on 2015.02.03..
 */
public class UzletDetailActivityCool : BaseActivity(), ObservableScrollViewCallbacks {

    private var mToolbar: View? = null
    private var mImageView: ImageView? = null
    private var mOverlayView: View? = null
    private var mScrollView: ObservableScrollView? = null
    private var mTitleView: TextView? = null
    private var mFab: View? = null
    private var mActionBarSize: Int = 0
    private var mFlexibleSpaceShowFabOffset: Int = 0
    private var mFlexibleSpaceImageHeight: Int = 0
    private var mFabMargin: Int = 0
    private var mToolbarColor: Int = 0
    private var mFabIsShown: Boolean = false

    private var headerRes: Int = 0
    private var contentTest: String? = null
    private var titleText: String? = null
    private var logoRes: Int = 0

    private var mLogoImageView: ImageView? = null
    private var mDetailTextView: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super<BaseActivity>.onCreate(savedInstanceState)
        setContentView(R.layout.activity_uzlet_detail_cool)

        setSupportActionBar(findViewById(R.id.toolbar) as Toolbar)

        headerRes = getIntent().getExtras().getInt(ARG_HEADER_RES)
        contentTest = getIntent().getExtras().getString(ARG_CONTENT)
        titleText = getIntent().getExtras().getString(ARG_TITLE)
        logoRes = getIntent().getExtras().getInt(ARG_LOGO)


        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height)
        mFlexibleSpaceShowFabOffset = getResources().getDimensionPixelSize(R.dimen.flexible_space_show_fab_offset)
        mActionBarSize = getActionBarSize()
        mToolbarColor = getResources().getColor(R.color.primary)

        mToolbar = findViewById(R.id.toolbar)
        if (!TOOLBAR_IS_STICKY) {
            mToolbar!!.setBackgroundColor(Color.TRANSPARENT)
        }

        mLogoImageView = findViewById(R.id.uzletDetailLogoImageView) as ImageView

        mLogoImageView!!.setImageDrawable(getDrawable(logoRes))

        mDetailTextView = findViewById(R.id.body) as TextView
        mDetailTextView!!.setText(Html.fromHtml(contentTest))


        mImageView = findViewById(R.id.image) as ImageView

        mImageView!!.setImageDrawable(getDrawable(headerRes))

        mOverlayView = findViewById(R.id.overlay)
        mScrollView = findViewById(R.id.scroll) as ObservableScrollView
        mScrollView!!.setScrollViewCallbacks(this)
        mTitleView = findViewById(R.id.title) as TextView
        mTitleView!!.setText(titleText)
        setTitle(null)
        mFab = findViewById(R.id.fab)
        mFab!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                Toast.makeText(this@UzletDetailActivityCool, "Kedvencekhez adva!", Toast.LENGTH_SHORT).show()
            }
        })
        mFabMargin = getResources().getDimensionPixelSize(R.dimen.margin_standard)
        ViewHelper.setScaleX(mFab, 0F)
        ViewHelper.setScaleY(mFab, 0F)

        ScrollUtils.addOnGlobalLayoutListener(mScrollView, object : Runnable {
            override fun run() {
                mScrollView!!.scrollTo(0, mFlexibleSpaceImageHeight - mActionBarSize)

                // If you'd like to start from scrollY == 0, don't write like this:
                //mScrollView.scrollTo(0, 0);
                // The initial scrollY is 0, so it won't invoke onScrollChanged().
                // To do this, use the following:
                //onScrollChanged(0, false, false);

                // You can also achieve it with the following codes.
                // This causes scroll change from 1 to 0.
                mScrollView!!.scrollTo(0, 1);
               // mScrollView!!.scrollTo(0, 0);
            }
        })
    }

    override fun onScrollChanged(scrollY: Int, firstScroll: Boolean, dragging: Boolean) {
        // Translate overlay and image
        val flexibleRange = (mFlexibleSpaceImageHeight - mActionBarSize).toFloat()
        val minOverlayTransitionY = mActionBarSize - mOverlayView!!.getHeight()
        ViewHelper.setTranslationY(mOverlayView, ScrollUtils.getFloat((-scrollY).toFloat(), minOverlayTransitionY.toFloat(), 0F))
        ViewHelper.setTranslationY(mImageView, ScrollUtils.getFloat((-scrollY / 2).toFloat(), minOverlayTransitionY.toFloat(), 0F))

        // Change alpha of overlay
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat(scrollY.toFloat() / flexibleRange, 0F, 1F))

        // Scale title text
        val scale = 1 + ScrollUtils.getFloat((flexibleRange - scrollY.toFloat()) / flexibleRange, 0F, MAX_TEXT_SCALE_DELTA)
        ViewHelper.setPivotX(mTitleView, 0F)
        ViewHelper.setPivotY(mTitleView, 0F)
        ViewHelper.setScaleX(mTitleView, scale)
        ViewHelper.setScaleY(mTitleView, scale)

        // Translate title text
        val maxTitleTranslationY = (mFlexibleSpaceImageHeight.toFloat() - mTitleView!!.getHeight().toFloat() * scale).toInt()
        var titleTranslationY = maxTitleTranslationY - scrollY
        if (TOOLBAR_IS_STICKY) {
            titleTranslationY = Math.max(0, titleTranslationY)
        }
        ViewHelper.setTranslationY(mTitleView, titleTranslationY.toFloat())

        // Translate FAB
        val maxFabTranslationY = mFlexibleSpaceImageHeight - mFab!!.getHeight() / 2
        val fabTranslationY = ScrollUtils.getFloat((-scrollY + mFlexibleSpaceImageHeight - mFab!!.getHeight() / 2).toFloat(), (mActionBarSize - mFab!!.getHeight() / 2).toFloat(), maxFabTranslationY.toFloat())
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            // On pre-honeycomb, ViewHelper.setTranslationX/Y does not set margin,
            // which causes FAB's OnClickListener not working.
            val lp = mFab!!.getLayoutParams() as FrameLayout.LayoutParams
            lp.leftMargin = mOverlayView!!.getWidth() - mFabMargin - mFab!!.getWidth()
            lp.topMargin = fabTranslationY.toInt()
            mFab!!.requestLayout()
        } else {
            ViewHelper.setTranslationX(mFab, mOverlayView!!.getWidth() - mFabMargin - mFab!!.getWidth().toFloat())
            ViewHelper.setTranslationY(mFab, fabTranslationY)
        }

        // Show/hide FAB
        if (fabTranslationY < mFlexibleSpaceShowFabOffset) {
            hideFab()
        } else {
            showFab()
        }

        if (TOOLBAR_IS_STICKY) {
            // Change alpha of toolbar background
            if (-scrollY + mFlexibleSpaceImageHeight <= mActionBarSize) {
                mToolbar!!.setBackgroundColor(ScrollUtils.getColorWithAlpha(1F, mToolbarColor))
            } else {
                mToolbar!!.setBackgroundColor(ScrollUtils.getColorWithAlpha(0F, mToolbarColor))
            }
        } else {
            // Translate Toolbar
            if (scrollY < mFlexibleSpaceImageHeight) {
                ViewHelper.setTranslationY(mToolbar, 0F)
            } else {
                ViewHelper.setTranslationY(mToolbar, (-scrollY).toFloat())
            }
        }
    }

    override fun onDownMotionEvent() {
    }

    override fun onUpOrCancelMotionEvent(scrollState: ScrollState) {
    }

    private fun showFab() {
        if (!mFabIsShown) {
            ViewPropertyAnimator.animate(mFab).cancel()
            ViewPropertyAnimator.animate(mFab).scaleX(1F).scaleY(1F).setDuration(200).start()
            mFabIsShown = true
        }
    }

    private fun hideFab() {
        if (mFabIsShown) {
            ViewPropertyAnimator.animate(mFab).cancel()
            ViewPropertyAnimator.animate(mFab).scaleX(0F).scaleY(0F).setDuration(200).start()
            mFabIsShown = false
        }
    }

    class object {
        public val ARG_HEADER_RES: String = "HEADER_ARG"
        public val ARG_CONTENT: String = "CONTENT_ARG"
        public val ARG_TITLE: String = "TITLE_ARG"
        public val ARG_LOGO: String = "LOGO_ARG"
        private val MAX_TEXT_SCALE_DELTA = 0.3.toFloat()
        private val TOOLBAR_IS_STICKY = false
    }
}