package hu.pont.mompark.ui.fragment


import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hu.pont.mompark.R;
import yalantis.com.sidemenu.interfaces.ScreenShotable
import android.graphics.Bitmap
import android.support.v4.app.Fragment
import android.graphics.Canvas
import com.vlad.android.kotlin.async
import android.widget.ImageView
import butterknife.bindView
import android.content.Intent
import android.net.Uri
import android.widget.TextView


/**
 * A simple {@link Fragment} subclass.
 */
public class KapcsolatFragment : Fragment(), ScreenShotable {

    private var containerView: View? = null
    private var bitmap: Bitmap? = null

    val locationMarkerImageView: ImageView by bindView(R.id.locationMarkerImageView)
    val momparkPhoneTextView: TextView by bindView(R.id.momparkPhoneTextView)


    override fun takeScreenShot() {
        async {
            val bitmap = Bitmap.createBitmap(containerView!!.getWidth(), containerView!!.getHeight(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            containerView!!.draw(canvas)
            this@KapcsolatFragment.bitmap = bitmap
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super<Fragment>.onViewCreated(view, savedInstanceState)
        this.containerView = view.findViewById(R.id.container)

        locationMarkerImageView!!.setOnClickListener { view ->
            var geoUri: String = "http://maps.google.com/maps?q=loc:47.490592,19.023972 (MOM park)"
            var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
            this@KapcsolatFragment.startActivity(intent);
        }

        momparkPhoneTextView!!.setOnClickListener{ view ->
            var intent: Intent = Intent(Intent.ACTION_DIAL);
            intent!!.setData(Uri.parse("tel:+3614875500"));
            this@KapcsolatFragment.startActivity(intent);
        }

    }

    override fun getBitmap(): Bitmap? {
        return bitmap
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_kapcsolat, container, false)
    }


    class object {
        public val KAPCSOLAT: String = "Kapcsolat"

    }

}
