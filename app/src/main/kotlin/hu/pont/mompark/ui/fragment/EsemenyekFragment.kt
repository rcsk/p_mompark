package hu.pont.mompark.ui.fragment


import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import hu.pont.mompark.R
import yalantis.com.sidemenu.interfaces.ScreenShotable
import android.graphics.Bitmap
import android.support.v4.app.Fragment
import android.widget.ListView
import java.util.ArrayList
import hu.pont.mompark.model.Esemeny
import hu.pont.mompark.adapter.EsemenyAdapter
import android.graphics.Canvas
import com.vlad.android.kotlin.async
import android.content.Intent
import java.util.Date

/**
 * A simple {@link Fragment} subclass.
 */
public class EsemenyekFragment : Fragment(), ScreenShotable {

    private var containerView: View? = null

    private var bitmap: Bitmap? = null

    protected var mEsemenyListView: ListView? = null

    val esemenyList = ArrayList<Esemeny>()
    var esemenyAdapter: EsemenyAdapter? = null
    var esemenyRootView: View? = null

    override fun takeScreenShot() {
        async {
            val bitmap = Bitmap.createBitmap(containerView!!.getWidth(), containerView!!.getHeight(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            containerView!!.draw(canvas)
            this@EsemenyekFragment.bitmap = bitmap
        }
    }


    override fun getBitmap(): Bitmap? {
        return bitmap
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super<Fragment>.onCreate(savedInstanceState)

        esemenyList.add(Esemeny("Karácsonyi fotozás", R.drawable.esemeny1, "Karácsonyi fotozás , mindenki mondja hogy csíz!"))
        esemenyList.add(Esemeny("Az ősz színes pillanatai", R.drawable.esemeny2, "Lorem ipsum dolores, Lorem ipsum dolores, Lorem ipsum dolores"))
        esemenyList.add(Esemeny("MOM Park Shpping days", R.drawable.esemeny3, "Lorem ipsum dolores, Lorem ipsum dolores, Lorem ipsum dolores"))

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        esemenyRootView = inflater.inflate(R.layout.fragment_esemenyek, container, false)

        return esemenyRootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super<Fragment>.onViewCreated(view, savedInstanceState)
        this.mEsemenyListView = view.findViewById(R.id.esemenyListView) as ListView
        this.containerView = view.findViewById(R.id.container)
        esemenyAdapter = EsemenyAdapter(getActivity(), R.layout.list_item_esemeny, esemenyList)
        mEsemenyListView!!.setAdapter(esemenyAdapter)


        mEsemenyListView!!.setOnItemClickListener({parent, view, position, id ->
           var esemeny: Esemeny =  ((parent as ListView)?.getAdapter() as EsemenyAdapter)?.getItem(position)
           putToCalendar(esemeny)
        })

    }

    fun putToCalendar(esemeny: Esemeny){
        var intent: Intent = Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("title", esemeny.getName());
        intent.putExtra("description", esemeny.getDesc());
        intent.putExtra("beginTime", (Date()).getTime());
        intent.putExtra("endTime", ((Date()).getTime()+3600L));
        startActivity(intent);
    }

    class object {
        public val ESEMENYEK: String = "Esemenyek"

    }

}
