package hu.pont.mompark.ui.fragment


import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import hu.pont.mompark.R
import yalantis.com.sidemenu.interfaces.ScreenShotable
import android.graphics.Bitmap
import android.support.v4.app.Fragment
import android.graphics.Canvas
import com.vlad.android.kotlin.async

/**
 * A simple {@link Fragment} subclass.
 */
public class VipFragment : Fragment(), ScreenShotable {

    private var containerView: View? = null
    private var bitmap: Bitmap? = null

    override fun takeScreenShot() {
        async {
            val bitmap = Bitmap.createBitmap(containerView!!.getWidth(), containerView!!.getHeight(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            containerView!!.draw(canvas)
            this@VipFragment.bitmap = bitmap
        }
    }

    override fun getBitmap(): Bitmap? {
        return bitmap
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_vip, container, false)
    }

    class object {
        public val VIPFRAGMENT: String = "Vip"

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super<Fragment>.onViewCreated(view, savedInstanceState)
        this.containerView = view.findViewById(R.id.container)
    }


}
