package hu.pont.mompark.model;

/**
 * Created by Richard Radics on 2015.02.03..
 */
public class Kupon {


    String name;
    int resId;


    public Kupon(String name, int resId) {
        this.name = name;
        this.resId = resId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Kupon kupon = (Kupon) o;

        if (resId != kupon.resId) return false;
        if (name != null ? !name.equals(kupon.name) : kupon.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + resId;
        return result;
    }
}
