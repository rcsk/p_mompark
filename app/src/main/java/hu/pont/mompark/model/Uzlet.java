package hu.pont.mompark.model;

/**
 * Created by Richard Radics on 2015.02.03..
 */
public class Uzlet {

    String name;
    int resId;
    Integer headerResId;
    String contentHtmlDesc;
    Integer logoResId;

    public static final String[] boltTipusLista = {"Cipő", "Bőráru", "Divat", "Élelmiszer", "Delikát", "Étterem", "Kávézó",
            "Könyv", "CD", "Írószer", "Sport", "Otthon", "Design", "Szabadidő", "Szépség", "Elektronika", "Egészség", "Szolgáltatás", "Optika", "Ékszer", "Óra"};


    public static String PAULANER_HTML_DESC = "<div align=\"center\"><p>A Paulaner Sörház igazi bajor sörrel és konyhával hozza elérhető távolságba a sörfesztiválok hangulatát. A bajor életérzés szerelmesei az elmúlt években gyakori vendégeinkké váltak, hiszen tudják: az eredeti Paulaner recept alapján készült sörök mellé ropogósra pirult csülkök, ínycsiklandó kolbászfélék, sörkorcsolyaként pedig perec dukál. A budai hegyek lábánál fekvő sörház meghitt, közvetlen környezetet biztosít a személyes találkozásokhoz. Különtermeink esküvők, születésnapok, céges karácsonyok, csapatépítő bulik, közös búfelejtések és ünneplések helyszínei. A foci megszállottjai nálunk barátaikkal együtt szurkolhatnak a vébé közvetítések alatt, és nem áll fenn az a veszély sem, hogy kifogynak a hideg sörökből. Az igazi Octoberfest-élményre vágyók számára ősszel sörünnepet szervezünk, Paulaner módra.</p>" +
            "<br /><br />" +
            "<b>Forgalmazott márkák / kínált szolgáltatások</b>" +
            "        <br />" +
            "Paulaner sörök, melegkonyhás vendéglátás, wifi, terasz, élőzene, rendezvények lebonyolítása, különtermek bérbeadása, sportesemények közvetítése" +
            "<br /><br />" +
            "<b>Elhelyezkedés</b><br />" +
            "Fórum szint | megtekintés a térképen ›" +
            "<br /><br />" +
            "<b>Nyitva tartás</b><br />" +
            "hétfő–szerda: 11:30-24:00<br />" +
            "csütörtök–szombat: 11:30-01:00<br />" +
            "vasárnap: 11:30-23:00<br />" +
            "<br />" +
            "<b>Elérhetőség</b><br />" +
            "tel: +36 1 201 5076<br />" +
            "fax: +36 1 201 4013<br />" +
            "mobil: +36 70 377 1000<br />" +
            "e-mail: info@paulansorhaz.hu,<br />paulanermompark@gmail.com<br />" +
            "domain: www.paulanersorhaz.hu<br /><br /></div>";

    public static String GRIFF_HTML_DESC = "<div align=\"center\"><p>A Griff Gentlemen’s már 25 éve a divat élvonalában, világmárkák széles kínálatával és folyamatosan bővülő választékával várja Önt.<br /> Női és férfi kollekcióink között minden stílust megtalál, legyen sportos vagy akár elegáns." +
            "<br /><br />" +
            "<b>Forgalmazott márkák / kínált szolgáltatások</b><br />" +
            "JOOP!, Daniel Hechter,<br /> Tommy Hilfiger, Camel,<br /> Levi’s, Hilfiger Denim,<br /> Marville, De Kuba,<br /> Street One, Basefield,<br /> Basefield Woman,<br /> Gentlemen, Exclusive by Griff,<br /> Trend by Griff, GRF Casual,<br /> Black Tie, Ladies by Griff\n" +
            "<br /><br />" +
            "<b>Elhelyezkedés</b><br />" +
            "Fórum szint | megtekintés a térképen ›<br /><br />" +
            "<b>Nyitva tartás</b><br />" +
            "hétfő–szombat: 10:00-21:00<br />" +
            "vasárnap: 10:00-19:00<br /><br />" +
            "<b>Elérhetőség</b><br />" +
            "tel: +36 1 487 5446<br />" +
            "e-mail: mom@griff.hu<br />" +
            "domain: www.griff.hu,<br />www.griffwebshop.com<br /><br />" +
            "<b>Üzletvezető</b><br />" +
            "Mészáros Attila<br /><br /></div>";


    public Uzlet(String name, int resId, Integer headerResId, String contentHtmlDesc, Integer logoResId) {
        this.name = name;
        this.resId = resId;
        this.headerResId = headerResId;
        this.contentHtmlDesc = contentHtmlDesc;
        this.logoResId = logoResId;
    }

    public Integer getLogoResId() {
        return logoResId;
    }

    public void setLogoResId(Integer logoResId) {
        this.logoResId = logoResId;
    }

    public Integer getHeaderResId() {
        return headerResId;
    }

    public void setHeaderResId(int headerResId) {
        this.headerResId = headerResId;
    }

    public String getContentHtmlDesc() {
        return contentHtmlDesc;
    }

    public void setContentHtmlDesc(String contentHtmlDesc) {
        this.contentHtmlDesc = contentHtmlDesc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Uzlet uzlet = (Uzlet) o;

        if (resId != uzlet.resId) return false;
        if (name != null ? !name.equals(uzlet.name) : uzlet.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + resId;
        return result;
    }

}
