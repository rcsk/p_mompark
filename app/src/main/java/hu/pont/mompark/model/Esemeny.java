package hu.pont.mompark.model;

/**
 * Created by Richard Radics on 2015.02.03..
 */
public class Esemeny {


    String name;
    String desc;
    int resId;


    public Esemeny(String name, int resId, String desc) {
        this.name = name;
        this.resId = resId;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Esemeny esemeny = (Esemeny) o;

        if (resId != esemeny.resId) return false;
        if (desc != null ? !desc.equals(esemeny.desc) : esemeny.desc != null) return false;
        if (name != null ? !name.equals(esemeny.name) : esemeny.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (desc != null ? desc.hashCode() : 0);
        result = 31 * result + resId;
        return result;
    }
}
