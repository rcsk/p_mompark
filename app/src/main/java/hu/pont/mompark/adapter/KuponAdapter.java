package hu.pont.mompark.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.List;

import hu.pont.mompark.R;
import hu.pont.mompark.model.Esemeny;
import hu.pont.mompark.model.Kupon;

/**
 * Created by Richard Radics on 2015.02.03..
 */
public class KuponAdapter extends ArrayAdapter<Kupon> {


    Context context;
    List<Kupon> kuponList;

    public KuponAdapter(Context context, int resource, List<Kupon> objects) {
        super(context, resource, objects);

        this.context = context;
        this.kuponList = objects;

    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Kupon current = kuponList.get(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.list_item_kupon, null);

        ImageView imageView = (ImageView)convertView.findViewById(R.id.kuponListItemImageView);

        imageView.setImageResource(current.getResId());

        return convertView;
    }
}
