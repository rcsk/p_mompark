package hu.pont.mompark.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import hu.pont.mompark.R;

/**
 * Created by Richard Radics on 2015.02.05..
 */
public class KuponChooseAdapter extends BaseAdapter {

    String[] osszetevoLista;

    Context context;

    public KuponChooseAdapter(String[] kuponok, Context context){
        this.osszetevoLista = kuponok;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_kupon_profil, null);
        }

        final View finalConvertView = convertView;
        ((CheckBox) (convertView).findViewById(R.id.osszetevoYesCheckbox)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ((CheckBox) (finalConvertView).findViewById(R.id.osszetevoNoCheckbox)).setChecked(false);
                }
            }
        });

        ((CheckBox) (convertView).findViewById(R.id.osszetevoNoCheckbox)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ((CheckBox) (finalConvertView).findViewById(R.id.osszetevoYesCheckbox)).setChecked(false);
                }
            }
        });


        ((TextView) (convertView).findViewById(R.id.osszetevoItemTextView)).setText(getItem(position));
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return osszetevoLista[position].hashCode();
    }

    @Override
    public String getItem(int position) {
        return osszetevoLista[position];
    }

    @Override
    public int getCount() {
        return osszetevoLista.length;
    }


}
