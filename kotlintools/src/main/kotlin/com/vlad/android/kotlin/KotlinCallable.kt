package com.vlad.android.kotlin

import java.util.concurrent.Callable

public inlineOptions(InlineOption.ONLY_LOCAL_RETURN) fun callable<T>(action: () -> T?): Callable<out T> {
    return object : Callable<T> {
        public override fun call(): T? = action()
    }
}
